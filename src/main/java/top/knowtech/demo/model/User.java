package top.knowtech.demo.model;

import lombok.Data;

@Data
public class User {
    int uid;
    String uname;
    String upass;
}
