package top.knowtech.demo.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @Autowired
    JdbcTemplate jdbc;


    @RequestMapping("/login")
    public String login(String uname, String upass, HttpServletRequest request, HttpServletResponse response) {
        int i = jdbc.queryForObject("select count(*) from users where uname=? and upass=?", Integer.class, new Object[]{uname, upass});
        if(i == 1) {
            HttpSession session =  request.getSession();
            session.setAttribute("uname", uname);

            Cookie cookie = new Cookie("uname", uname);
            cookie.setMaxAge(60*60*24*30);
            response.addCookie(cookie);

            return "default";
        } else {
            return "fail";
        }
    }

 //   @ResponseBody
    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String uname = (String)session.getAttribute("uname");
        model.addAttribute("uname", uname);
        //return uname;
        return "index";
    }

    @RequestMapping("/success")
    public String success() {
        return "success";
    }   

}
