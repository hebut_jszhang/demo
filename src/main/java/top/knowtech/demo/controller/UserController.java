package top.knowtech.demo.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import top.knowtech.demo.model.User;

@Controller
public class UserController {
    
    @Autowired
    JdbcTemplate jdbc;


    @RequestMapping("/adduser")
    public String adduser() {
        return "adduser";
    }

    @RequestMapping("/add") 
    public String add(String uname,String upass, String reupass, Model model) {
        if(!upass.equals(reupass)) {
            return "fail";
        }
        if(uname.length() < 2) {
            return "fail";
        }
        
        int check  = jdbc.queryForObject("select count(*) from users where uname= ?", Integer.class, new Object[]{uname});
        if (check > 0) {
            model.addAttribute("msg", "用户已存在");
            return "fail";
        } else {
            int i  = jdbc.update("insert into users values(null ,?,?) ", new Object[] {uname,upass});

            if (i >0) {
                return "success";
            } else {
                return "fail";
            }
        }
    }


    @RequestMapping("/usercount")
    public String usercount(Model model, HttpServletRequest request) {

        int count = jdbc.queryForObject("select count(*) from users", Integer.class);
        model.addAttribute("count", count);

        HttpSession session = request.getSession();
        String uname = (String) session.getAttribute("uname");
        model.addAttribute("uname", uname);
        return "usercount";
    }

    @RequestMapping("/showuser")
    public String showuser(Model model, HttpServletRequest request) {
        List<User> users = jdbc.query("select * from users", new BeanPropertyRowMapper<User>(User.class));
        model.addAttribute("users", users);
        model.addAttribute("count", users.size());
        
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals("uname")) {
                model.addAttribute("uname", cookie.getValue());
            }
        }

        return "showuser";
    }

}
